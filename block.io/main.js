

let context, player1, loop, asteroids, size, spawnx, spawny, gameover;

/////////////HUGAWAN KAAYO KO MU CODE SUPPER///////////////
/////////// <3 <3 <3  NEW VARIABLES HERE  <3 <3 <3////////////
let  i = 0;
let timer = 0;
context = document.querySelector("canvas").getContext("2d");

context.canvas.height = 600;
context.canvas.width = 800;

context.fillStyle = "#202020"; //background sa game na color
context.fillRect(0, 0, 800, 600);// x, y, width, height, dimension sa background

gameover = false; // set to false sa by default

size = Math.floor((Math.random() * 100) + 1); // random size generator sa box box or asteroid
spawnx = Math.floor((Math.random() * 800) + 1);//random generator sa coordinates sa mga box box
spawny = Math.floor((Math.random() * 600) + 1);
//ikaw ni, ang red nga ga lihok lihok
player1 = {

  height:10,
  width:10,
  x:400, // center of the canvas
  x_velocity:0,
  y:300,
  y_velocity:0

};




player1controls = {

  left:false,
  right:false,
  up:false,
  down:false,

  keyListener:function(event) {

    var key_state = (event.type == "keydown")?true:false;

    switch(event.keyCode) {

      case 37:// 37 ang value sa arrow left left key na basahon sa code para ka gets sya na mao na na key ang gituplok
              // same sa tanan other keys
        player1controls.left = key_state;
      break;
      case 38:// up key
        player1controls.up = key_state;
      break;
      case 39:// right key
        player1controls.right = key_state;
      break;
      case 40:// down key
        player1controls.down = key_state;
      break;
    }

  }

};






loop = function() {


  if (player1controls.up) {

    player1.y_velocity -= 0.5;

  }

  if (player1controls.left) {

    player1.x_velocity -= 0.5;

  }

  if (player1controls.right) {

    player1.x_velocity += 0.5;

  }

  if (player1controls.down) {

    player1.y_velocity += 0.5; // baliktad ilahang cartesian coordinate plane besh
                                // down man so dapat negative but anyway positive jud sya ang value para munaog

  }


  player1.x += player1.x_velocity; // mao ni ang real pangpalihok
  player1.y += player1.y_velocity; // 
  player1.x_velocity *= 0.9;// friction
  player1.y_velocity *= 0.9;// friction





  // if player1 goes below floor line
  if (player1.y > 600 - 10) {

    player1.y = 0;

  }

  if (player1.y < 0 - 10) {

    player1.y = 600 - 10;

  }

  // if player1 is going off the left of the screen
  if (player1.x < -10) {

    player1.x = 800;

  } else if (player1.x > 800) {// if player1 goes past right boundary
    player1.x = -10;

  }
//////////////////////////////
  



  
  context.fillStyle = "#ff0000";// hex for red

  context.fillRect(player1.x, player1.y, player1.width, player1.height);
  context.fill();
  context.strokeStyle = "#202830";



  ////////////////ASTEROIDS/////////////////////
  for (i; i!=20; i++){

 


    asteroids = {


    height:  Math.floor((Math.random() * 100) + 20),
    width: Math.floor((Math.random() * 100) + 20),
    x:Math.floor((Math.random() * 800) + 1), // center of the canvas
    x_velocity: Math.floor((Math.random() * 5) + 1),
    y:Math.floor((Math.random() * 600) + 1),
    y_velocity: Math.floor((Math.random() * 5) + 1),  
    }
    

    asteroids.x += asteroids.x_velocity;
    asteroids.y += asteroids.y_velocity;

    context.fillStyle = "#406286"
    context.fillRect(asteroids.x, asteroids.y, asteroids.width, asteroids.height);
    context.beginPath();
  }
//////////COLLISIONS////////////////////
// if asteroids goes below floor line
if (asteroids.y > 600 - 10) {

  asteroids.y = 0;

}

if (asteroids.y < 0 - 10) {

  asteroids.y = 600 - 10;

}

// if asteroids is going off the left of the screen
if (asteroids.x < -10) {

  asteroids.x = 800;

} else if (asteroids.x > 800) {// if asteroids goes past right boundary

  asteroids.x = -10;

}
//////////////game control ug ma game over or maka next level////////////////////



    
    if(timer>=800){
    console.log("checking stop parameter");
    window.alert("GAMEOVER")
    gameover=true;
    location.reload();
   
    
  }
 


if (gameover==false){

      
  timer +=1;
  console.log(timer);
  let blabla = document.getElementById("timer");
  blabla.innerHTML = timer;

  
}




if (player1.x < asteroids.x + asteroids.width  && player1.x + player1.width  > asteroids.x &&
  player1.y < asteroids.y + asteroids.height && player1.y + player1.height > asteroids.y) {

    gameover = true;
    window.alert("YEHEY");
    level +=1;
   location.reload();


}
/////////////////////////////////////////////////////


  // call update when the browser is ready to draw again
  if (gameover==false){

  
    
    window.requestAnimationFrame(loop);
    
    
  }
  

};

window.addEventListener("keydown", player1controls.keyListener)
window.addEventListener("keyup", player1controls.keyListener);
window.requestAnimationFrame(loop);
